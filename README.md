# OHMC4 On Head Motion Controller



On Head Motion Controller 4, OHMC4 for short, is a dedicated 4 Nozzle Motion Controller Kit!
As the name implies and due to its small form factor, the main controller is stack upon its power board in a 38mm X 53mm X 20mm size,
and seats on your gantry Head, reliefing the X axis from some cable loads, uses flex cables to connect to the nozzle bords and connector board. The "4" number is for the Nozzle count!

OHMC board

- DCDC 24 to 12V convertor
- 5 V LDO
- Digital USB isolator
- 2 downstream USB Hub
- SPI Axis outputs
- Closed loop X and Y Axis
- 4 independent Z Axis outputs
- 4 C Axis outputs
- 4 vacuum sensor inputs
- 2 X 4 Actuator drivers
- On board usb hub for the down looking camera
- Down looking Camera light driver switch
- CAN FD

Nozzle board (1 per nozzle)

- 2 Stepper drivers, Z & C
- End Stop
- Vacuum sensor low noise high quality pre amp output
- Solenoid valve driver input
- Aux driver input


Connector board

- 35V power regulator for X & Y stepper drivers
- USB hub for Up Looking camera & OHMC4
- Main USB Port
- CAN Port
- Power connector


OHMC firmware is currently in development !!



